import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

//const apiUrl = 'http://35.239.158.67:8005/api/';
//const apiUrl = 'http://localhost:8181/peka_midleware_sqlsrv/public/api/';
//const apiUrl = 'https://apps.pertamina.com/api/mobile_peka/public/api/';
//const apiUrl = 'http://localhost:8000/api/';
const apiUrl = 'http://35.222.54.40/peka_midleware_sqlsrv/public/api/';

const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Headers':'Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods':'POST, GET, OPTIONS, PUT',
  'Content-Type':'application/json'
})
};


const httpOptionsForm = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Headers':'Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods':'POST, GET, OPTIONS, PUT',
  'Content-Type':'multipart/form-data; boundary=WebAppBoundary'
})
};

@Injectable({
  providedIn: 'root'
})
export class ObservasiService {

  constructor(private http: HttpClient) { }


  login(data): Observable<any> {
    /*
    PARAM:
      UserName
      Password
    */

   try {
    return this.http.post(apiUrl+'PekaEmployee/get', data, httpOptions);

  } catch (error) {
    console.log(error.status);
    console.log(error.error); // Error message as string
    console.log(error.headers);
  }

   
  }

  list(data): Observable<any> {
    /*
    PARAM:
      UserName
      filter
      costcenter
    */

   try {
    return this.http.post(apiUrl+'TObservasi/list', data, httpOptions);

  } catch (error) {
    console.log(error.status);
    console.log(error.error); // Error message as string
    console.log(error.headers);
  }

   
  }


  get(data): Observable<any> {
    /*
    PARAM:
      IDobservasion
    */
   

    try {
      return this.http.post(apiUrl+'TObservasi/get'
    , data, httpOptions);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }

  delete(data): Observable<any> {
    /*
    PARAM:
      IDobservasion
    */
    

    try {
      return this.http.post(apiUrl+'TObservasi/delete'
      , data, httpOptions);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }


  insert(data): Observable<any> {
    /*
    PARAM:
      //IDobservasion:

      IDklasifikasi:2
spesifikasi:3
DateObs:""2019-12-10""
Pengamatan:5
Lanjutan:6
Klasifikasi:1017
FilePhoto:file,file2,file3
IDNIK:9
NamaEmploye:10
FungsiName:11
Email:isparthama@gmail.com
NoTlp:13

//IsActive:14

CreateID:mk.lia.amelia
AreaID:17
jabatan:18
processApl:110
langsung:20

//PICNIK:21
//PICSign:22
//PISignDate:2019-10-29
//PICEmail:24
//PICInformasi:25
//RiskA:26
//RiskB:27
//RejectReason:28
//Pengelolahinfor:29
//UserBypass:30
//BypassDate:2019-10-29
//Aksi:32
//AksiDate:2019-10-29
//AksiComment:34
//CostCenter:35

unsafeDetailId:1,2,3,4

    */
   
    try {
      return this.http.post(apiUrl+'TObservasi/insert'
    , data);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }


  update(data): Observable<any> {
    /*
    PARAM:
     IDobservasion:1215
IDklasifikasi:2
spesifikasi:3
DateObs:2019-10-29
Pengamatan:5
Lanjutan:6
Klasifikasi:7
FilePhoto:8
IDNIK:9
NamaEmploye:10
FungsiName:11
Email:isparthama@gmail.com
NoTlp:13
IsActive:14
CreateID:2019-10-29
AreaID:17
jabatan:18
processApl:110
langsung:20
PICNIK:21
PICSign:22
PISignDate:2019-10-29
PICEmail:24
PICInformasi:25
RiskA:26
RiskB:27
RejectReason:28
Pengelolahinfor:29
UserBypass:30
BypassDate:2019-10-29
Aksi:32
AksiDate:2019-10-29
AksiComment:34
CostCenter:35
unsafeDetailId:9,8,7,6
    */
  
    try {
      return this.http.post(apiUrl+'TObservasi/update'
      , data);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }



  listArea(): Observable<any> {
    

    try {
      return this.http.post(apiUrl+'TArea/list_area'
      , [], httpOptions);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }
  
  listSubArea(data): Observable<any> {
    /*param
      KlarifikasiID
    */
   

    try {
      return this.http.post(apiUrl+'TSubArea/list_sub_area'
    , data, httpOptions);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }
  listUnsafe(): Observable<any> {
    /*param
    */
   

    try {
      return this.http.post(apiUrl+'TUnsafe/list'
      , [], httpOptions);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }

  listUnsDetail(data): Observable<any> {
    /*param
      UnsafeID
    */
   

    try {
      return this.http.post(apiUrl+'TUnsDetail/list'
      , data, httpOptions);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }
  
  listEmployee(): Observable<any> {
    /*param
    */
   

    try {
      return this.http.post(apiUrl+'PekaEmployee/list'
    , [], httpOptions);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }

  
  listPostCenter(): Observable<any> {
    /*param
    */
  

    try {
      return this.http.post(apiUrl+'PekaCostCenter/list'
      , [], httpOptions);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }


  
}
