import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { GlobalService } from './services/global.service';
import { Storage } from '@ionic/storage';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  

  login: any = '';
  userData: any = '';
  userName = '';

  public appPages = [
    {
      title: 'List Observasi',
      url: '/home',
      icon: 'document'
    },
    {
      title: 'FAQ',
      url: '/faq',
      icon: 'paper'
    },
    {
      title: 'Logout',
      url: '/logout',
      icon: 'log-out'
    }
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public storage: Storage,
    public global: GlobalService,
    public router: Router
  ) {
    this.initializeApp();

 }

 
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.

    this.router.events.subscribe(event =>{
      if (event instanceof NavigationStart){
         this.routerChangeMethod();
      }
   })

   

  }


  routerChangeMethod(){
    console.log('change route');
    
    this.storage.get('login').then((val) => {
      this.login = val;
    });
    this.storage.get('userData').then((val) => {
      this.userData = val;
    });
  }
  
 
}
