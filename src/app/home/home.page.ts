import { ObservasiService } from './../services/observasi.service';
import { Component, ElementRef } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { Platform } from '@ionic/angular';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  obs = [];
  obsDummy = [];
  userData: any = [];
  segment = 0;

  constructor(private ObservasiProvider: ObservasiService,
   public global: GlobalService,
    private storage: Storage,
    private router: Router,
    private elementRef: ElementRef,
    private platform: Platform
    ) {
    
  }


  segmentChanged(ev: any) {
    console.log('', ev.detail.value);
    console.log('segment : ', this.segment);
    this.getObs(this.userData.UserName,ev.detail.value,this.userData.CostCenter);
  }

  

  ngOnInit(): void {
   
    
    this.storage.get('userData').then((val) => {
      this.userData = val;
      console.log(this.userData);   
      console.log(this.userData.UserName);    
      this.getObs(this.userData.UserName,0,this.userData.CostCenter);
    });

    setTimeout(() => { 
      this.getObs(this.userData.UserName,0,this.userData.CostCenter);
    }, 1500);
  
  }
  
  getInitData(): void {
    this.userData = localStorage.getItem('userData');
    this.getObs(this.userData.UserName,0,this.userData.CostCenter);
    console.log(localStorage.getItem('userData'));

  }

  getObs(username, filter, costcenter): void { 
    this.global.showLoader();
    var data = {UserName: username,
                filter:filter,costcenter:costcenter};
    
    this.ObservasiProvider.list(data).subscribe( ress =>
        {
            this.obs = ress.data;
            this.global.hideLoader();
            console.log(this.obs);

        }
      );

      
  }



  ngOndestroy() {
    this.elementRef.nativeElement.remove();
  }







}

  


