export interface Iemploye { 
    OlahID: number,
    PICID: number,
    NOPEK: string,
    PersonalID: number,
    Npassword: string,
    NIK: string,
    CostCenter: string,
    TLahir: string,
    TemLahir: string,
    StatEmploye: string,
    IsActive: number,
    CreatedBy: string,
    UserName: string,
    Nama: string,
    Email: string,
    CCName: string
  }
  