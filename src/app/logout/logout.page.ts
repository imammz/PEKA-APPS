import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {

  constructor(private router: Router,
    public storage: Storage,
    public global: GlobalService) { }

  ngOnInit() {
    
    this.storage.set('login', false); 
    this.storage.set('userData', ''); 
    this.storage.set('userLdap', ''); 

    this.global.showLoader();


    setTimeout(() => { 
      this.global.hideLoader();
      this.router.navigate(['/login']);
    }, 1500);

  }

}
