import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ObservasiService } from './../services/observasi.service';
import { Router } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/Camera/ngx';
import { ActionSheetController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { HttpClient } from '@angular/common/http';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Storage } from '@ionic/storage';
import { FilePath } from '@ionic-native/file-path/ngx';
 
const today = new Date();
const STORAGE_KEY = 'tambah_obs_'+today.getFullYear()+''+(today.getMonth()+1)+''+today.getDate()+today.getHours() + '' + today.getMinutes() + '' + today.getSeconds();
@Component({
  selector: 'app-tambah-obs',
  templateUrl: './tambah-obs.page.html',
  styleUrls: ['./tambah-obs.page.scss'],
})
export class TambahObsPage implements OnInit {

  loader = null;
  KlarifikasiID = '';
  UnsafeID= '';
  userData: any = [];
  error = 0;
  msg = '';

  area = [];
  subarea = [];
  unsafe=[];
  unsafedetail=[];
  pegawai=[];
  CostCenter = [];
  jenisPekerja = 'pekerja';

  unsafedetailVal = [];
  processApl = 0;
  indexunsafe = 0;

  unsafeDetailC = [];

  arrayImg = [];
  arrayImgName = [];

  param = {
    IDklasifikasi:'',
    spesifikasi:'',
    DateObs:'',
    Pengamatan:'',
    Lanjutan:'',
    Klasifikasi:'',
    FilePhoto:'',
    IDNIK:'',
    NamaEmploye:this.userData.Nama,
    FungsiName:'',
    Email:'',
    NoTlp:'',
    CreateID:this.userData.UserName,
    AreaID:'',
    jabatan:'',
    processApl:'',
    langsung:'',
    unsafeDetailId:'',
    CostCenter:'',
    UnsafeID:'1016',
    attach_file_1: null,
    attach_file_2: null,
    attach_file_3: null
  };



  constructor(
    private ObservasiProvider: ObservasiService,
    public loadingController: LoadingController,
    private router: Router,
    private global: GlobalService,
    private storage: Storage,
    private camera: Camera, private file: File, private http: HttpClient, private webview: WebView,
    private actionSheetController: ActionSheetController, private toastController: ToastController,
     private platform: Platform, 
    private ref: ChangeDetectorRef, private filePath: FilePath
    ) { }



    images = [];
  
  ngOnInit() {
    this.listArea();
    this.listUnsafe();

    this.listPostCenter();

    this.platform.ready().then(() => {
     // this.loadStoredImages();
    });

    setTimeout(() => { 
      this.listEmploye();
    }, 1500);

   
  }

  croppedImagepath = "";
  isLoading = false;

  imagePickerOptions = {
    maximumImagesCount: 1,
    quality: 50
  };


  loadStoredImages() {
    this.storage.get(STORAGE_KEY).then(images => {
      if (images) {
        let arr = JSON.parse(images);
        this.images = [];
        for (let img of arr) {
          let filePath = this.file.dataDirectory + img;
          let resPath = this.pathForImage(filePath);
          this.images.push({ name: img, path: resPath, filePath: filePath });
        }
      }
    });
  }
 
  pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      let converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }
 
  async presentToast(text) {
    const toast = await this.toastController.create({
        message: text,
        position: 'bottom',
        duration: 3000
    });
    toast.present();
  }

  pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then(imagePath => {
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
          this.filePath.resolveNativePath(imagePath)
              .then(filePath => {
                  let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                  let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                  this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
              });
      } else {
          var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
          var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
          this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
  });
  }

  async selectImage() {

    if(this.images.length > 2) {
      alert('Maksimum Gambar 3');
    }
    else { 
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();


    setTimeout(() => { 
      this.upload();
    },1500);


    }
  }


  takePicture(sourceType: PictureSourceType) {
    var options: CameraOptions = {
        quality: 100,
        sourceType: sourceType,
        saveToPhotoAlbum: false,
        correctOrientation: true
    };
 
    this.camera.getPicture(options).then(imagePath => {
        if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
            this.filePath.resolveNativePath(imagePath)
                .then(filePath => {
                    let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
                });
        } else {
            var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        }
    });
 
}
  createFileName() {
    var d = new Date(),
        n = d.getTime(),
        newFileName = n + ".png";
    return newFileName;
}
 
copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
        this.updateStoredImages(newFileName);
    }, error => {
        this.presentToast('Error while storing file.');
    });
}
 
updateStoredImages(name) {
    this.storage.get(STORAGE_KEY).then(images => {
        let arr = JSON.parse(images);
        if (!arr) {
            let newImages = [name];
            this.storage.set(STORAGE_KEY, JSON.stringify(newImages));
        } else {
            arr.push(name);
            this.storage.set(STORAGE_KEY, JSON.stringify(arr));
        }
 
        let filePath = this.file.dataDirectory + name;
        let resPath = this.pathForImage(filePath);
 
        let newEntry = {
            name: name,
            path: resPath,
            filePath: filePath
        };
 
        this.images = [newEntry, ...this.images];
        this.ref.detectChanges(); // trigger change detection cycle
    });
}


deleteImage(imgEntry, position) {
    this.images.splice(position, 1);
 
    this.storage.get(STORAGE_KEY).then(images => {
        let arr = JSON.parse(images);
        let filtered = arr.filter(name => name != imgEntry.name);
        this.storage.set(STORAGE_KEY, JSON.stringify(filtered));
 
        var correctPath = imgEntry.filePath.substr(0, imgEntry.filePath.lastIndexOf('/') + 1);
 
        this.file.removeFile(correctPath, imgEntry.name).then(res => {
            this.presentToast('File removed.');
        });
    });
}

 
startUpload(imgEntry) {
  this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
      .then(entry =>  {
          ( < FileEntry > entry).file(file => this.readFile(file))
      })
      .catch(err => {
          this.presentToast('Error while reading file.');
      });
}

readFile(file: any) {
  const reader = new FileReader();
  reader.onload = () => {
      const formData = new FormData();
      const imgBlob = new Blob([reader.result], {
          type: file.type
      });

   



    
  };
  reader.readAsArrayBuffer(file);
}

  
 
  listArea(): void { 
   // this.global.showLoader();


      this.ObservasiProvider.listArea().subscribe( ress =>
        {
            this.area = ress.data;
           //  this.global.hideLoader();
            console.log(JSON.stringify(this.area, null, 4));

            

        }
      );
      

  }


  listSubArea(idk): void { 
    console.log(JSON.stringify(idk, null, 4));
    var ID = idk;

    this.subarea = [];
    this.param.AreaID = '';

    this.global.showLoader();
    this.ObservasiProvider.listSubArea({KlarifikasiID:ID}).subscribe( ress =>
        {
            this.subarea = ress.data;
            this.global.hideLoader();
            console.log(JSON.stringify(this.subarea, null, 4));

            

        }
      );
  }


  listUnsafe(): void { 
   // this.global.showLoader();

    this.ObservasiProvider.listUnsafe().subscribe( ress =>
        {
            this.unsafe = ress.data;
            
           //  this.global.hideLoader();
            console.log(JSON.stringify(this.unsafe, null, 4));
            console.log(JSON.stringify(this.unsafeDetailC, null, 4));
        }
      );
  }


  listUnsafeDetail(idk): void { 

    console.log(JSON.stringify(idk, null, 4));
    var ID = idk;
 
    this.unsafedetail = [];
    this.param.unsafeDetailId = '';

    this.global.showLoader();
    this.ObservasiProvider.listUnsDetail({UnsafeID:ID}).subscribe( ress =>
        {
            this.unsafedetail = ress.data;
             this.global.hideLoader();
           // console.log(JSON.stringify(Object.entries(ress.data));
            console.log(JSON.stringify(this.unsafedetail, null, 4));

        }
      );
  }

  listEmploye(): void { 
   // this.global.showLoader();
    this.ObservasiProvider.listEmployee().subscribe( ress =>
        {
            this.pegawai = ress.data;
           //  this.global.hideLoader();
            console.log(JSON.stringify(this.pegawai, null, 4));

           
            this.storage.get('userData').then((val) => {
              this.userData = val;
              this.param.CreateID    = this.userData.UserName;
              this.param.Email    = this.userData.Email;
              this.param.NamaEmploye = this.userData.Nama;
              this.param.CostCenter = this.userData.CostCenter;
      
              console.log(JSON.stringify(this.param.NamaEmploye, null, 4));
            });

        }
      );
  }


  listPostCenter(): void { 
    // this.global.showLoader();
     this.ObservasiProvider.listPostCenter().subscribe( ress =>
         {
             this.CostCenter = ress.data;
            //  this.global.hideLoader();
           
             console.log(JSON.stringify(this.CostCenter, null, 4));
         }
       );
   }


   upload(): void{

    
    for (let img of this.images) { 
      this.startUpload(img);
   }



   this.param.FilePhoto = this.arrayImgName.join(',');


   //alert(this.param.FilePhoto);

   } 

   simpan(proses): void{

    console.log(JSON.stringify(this.images, null, 4));

   
  
    if(this.param.IDklasifikasi == '') {
        this.error = 1;
        this.msg += 'Area Tidak Boleh Kosong; \n';
    }
    if(this.param.spesifikasi == '') {
        this.error = 1;
        this.msg += 'Lokasi Tidak Boleh Kosong; \n';
    }
    if(this.param.NamaEmploye == '') {
        this.error = 1;
        this.msg += 'Nama Tidak Boleh Kosong; \n';
    }

    console.log(JSON.stringify(this.param, null, 4));
    console.log(JSON.stringify(this.param.FilePhoto, null, 4));
    //alert(this.param.FilePhoto);

    const formData = new FormData();

        
    Object.entries(this.images).
    forEach(([key, value]) => { 
      
      console.log('File : '+JSON.stringify(key, null, 4)); 
      console.log(JSON.stringify(value, null, 4)); 
     
      if(key == '0') {

        this.file.resolveLocalFilesystemUrl(value.filePath)
        .then(entry =>  {
            ( < FileEntry > entry).file(file => {
              const reader = new FileReader();
              reader.onload = () => {
                 
                  const imgBlob = new Blob([reader.result], {
                      type: file.type
                  });
               formData.append('attach_file_1', imgBlob, file.name);
               console.log('Key '+key+' - '+formData); 
               console.log(JSON.stringify(imgBlob, null, 4)); 
              };
              reader.readAsArrayBuffer(file);
            })
        })
        .catch(err => {
            this.presentToast('Error while reading file.');
        });

      } 
      if(key == '1') {
        this.file.resolveLocalFilesystemUrl(value.filePath)
        .then(entry =>  {
            ( < FileEntry > entry).file(file => {
              const reader = new FileReader();
              reader.onload = () => {
               
                  const imgBlob = new Blob([reader.result], {
                      type: file.type
                  });
               formData.append('attach_file_2', imgBlob, file.name);
               console.log('Key '+key+' - '+formData);
               console.log(JSON.stringify(imgBlob, null, 4)); 
              };
              reader.readAsArrayBuffer(file);
            })
        })
        .catch(err => {
            this.presentToast('Error while reading file.');
        });
      }
      if(key == '2') {
        this.file.resolveLocalFilesystemUrl(value.filePath)
        .then(entry =>  {
            ( < FileEntry > entry).file(file => {
              const reader = new FileReader();
              reader.onload = () => {

                  const imgBlob = new Blob([reader.result], {
                      type: file.type
                  });
               formData.append('attach_file_3', imgBlob, file.name);
               console.log('Key '+key+' - '+formData);
               console.log(JSON.stringify(imgBlob, null, 4)); 
              };
              reader.readAsArrayBuffer(file);
            })
        })
        .catch(err => {
            this.presentToast('Error while reading file.');
        });
      }    
    });
 

    setTimeout(() => { 
     
      if(this.param.IDklasifikasi == '' || this.param.spesifikasi == '' || this.param.NamaEmploye == '' ) {
        this.global.presentAlert('Error','Gagal Insert',this.msg);
      }
      else {
          
      var tempUnsafedetail = [];
      this.param.processApl = proses;
  
      var i= 0;
      for (let [key, value] of Object.entries(this.unsafeDetailC)) {
         tempUnsafedetail[i] = key;
         i++;
      }
  
      this.param.unsafeDetailId = tempUnsafedetail.toString();
      this.param.Klasifikasi = this.param.UnsafeID;
  
      
      this.global.showLoader();

      try {

      

        formData.append('IDklasifikasi', this.param.IDklasifikasi);
        formData.append('spesifikasi', this.param.spesifikasi);
        formData.append('DateObs', this.param.DateObs);
        formData.append('Pengamatan', this.param.Pengamatan);
        formData.append('Lanjutan', this.param.Lanjutan);
        formData.append('Klasifikasi', this.param.Klasifikasi);
        formData.append('FilePhoto', this.param.FilePhoto);
       
        formData.append('IDNIK', this.param.IDNIK);
        formData.append('NamaEmploye', this.param.NamaEmploye);
        formData.append('FungsiName', this.param.FungsiName);
        formData.append('Email', this.param.Email);
        formData.append('NoTlp', this.param.NoTlp);
        formData.append('CreateID', this.param.CreateID);
        formData.append('AreaID', this.param.AreaID);
        formData.append('jabatan', this.param.jabatan);
        formData.append('processApl', this.param.processApl);
        formData.append('langsung', this.param.langsung);
        formData.append('unsafeDetailId', this.param.unsafeDetailId);
        formData.append('CostCenter', this.param.CostCenter);
        formData.append('UnsafeID', this.param.UnsafeID);

        console.log('Kirim : '+JSON.stringify(formData, null, 4));
        this.ObservasiProvider.insert(formData).subscribe( ress =>
          {
            this.global.hideLoader();
            this.router.navigate(['/home']);
            console.log('Response : '+JSON.stringify(ress, null, 4));
          }
        );
    } catch (error) {
        this.global.presentAlert('Error','Edit Data Gagal',' — Error is handled gracefully: '+ error.name);
        this.global.hideLoader();
      }
    
      }

    }, 2000);

   
    
  }



    comboEmploye(id): void { 
        this.pegawai.forEach(el => {
           if(el.Nama==id) {
             this.param.Email = el.Email;
           }
        });
     }


  cancel(): void{
    this.router.navigateByUrl('/home');
  }
  


}
