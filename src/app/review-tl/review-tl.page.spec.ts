import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReviewTlPage } from './review-tl.page';

describe('ReviewTlPage', () => {
  let component: ReviewTlPage;
  let fixture: ComponentFixture<ReviewTlPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewTlPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReviewTlPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
