import { Component, OnInit } from '@angular/core';
import { ObservasiService } from '../services/observasi.service';
import { LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-review-tl',
  templateUrl: './review-tl.page.html',
  styleUrls: ['./review-tl.page.scss'],
})
export class ReviewTlPage implements OnInit {


  loader = null;
  KlarifikasiID = '';
  UnsafeID= '';
  userData: any = [];
  error = 0;
  msg = '';
  IDobservasion = '';
  opsi = "1";

  area = [];
  subarea = [];
  unsafe=[];
  unsafedetail=[];
  pegawai=[];
  CostCenter = [];
  jenisPekerja = 'pekerja';

  unsafedetailVal = [];
  processApl = 0;
  indexunsafe = 0;

  unsafeDetailC = [];


  param = {
    IDklasifikasi:'',
    spesifikasi:'',
    DateObs:'',
    Pengamatan:'',
    Lanjutan:'',
    Klasifikasi:'',
    FilePhoto:'',
    IDNIK:'',
    NamaEmploye:'',
    FungsiName:'',
    Email:'',
    NoTlp:'',
    CreateID:'',
    AreaID:'',
    jabatan:'',
    processApl:0,
    langsung:'',
    unsafeDetailId:'',
    CostCenter:'',
    UnsafeID:'',
    lokasi_tempat:'',
    processApl_desc: '',
    IDobservasion: '',
    IsActive: null,
    CreateDate: '',
    PICNIK: null,
    PICSign: null,
    PISignDate: '',
    PICEmail: null,
    PICInformasi: null,
    RiskA: null,
    RiskB: null,
    RejectReason: null,
    Pengelolahinfor: null,
    UserBypass: null,
    BypassDate: '',
    Aksi: null,
    AksiDate: '',
    AksiComment: null,
    unsafedetail: []
  };

  


  constructor( private ObservasiProvider: ObservasiService,
    public loadingController: LoadingController,
    private router: Router,
    public global: GlobalService,
    private routeAct: ActivatedRoute,
    private storage: Storage) { }

  ngOnInit() {
    
    this.IDobservasion = this.routeAct.snapshot.paramMap.get("id");
    
    console.log(this.IDobservasion);

    this.listArea();
    this.listUnsafe();
    this.listPostCenter();

    this.storage.get('userData').then((val) => {
      this.userData = val;
      this.param.CreateID    = this.userData.UserName;
      this.param.Email    = this.userData.Email;
    });

  }


  getObsById(): void { 
    this.global.showLoader();
    var data = {IDobservasion: this.IDobservasion};
    
    this.ObservasiProvider.get(data).subscribe( ress =>
        {
            this.param = ress.data[0];
            this.global.hideLoader();
            console.log(this.param); 

            this.param.UnsafeID = this.param.Klasifikasi;
          


        }
      );
  }


  listArea(): void { 
   // this.global.showLoader();


      this.ObservasiProvider.listArea().subscribe( ress =>
        {
            this.area = ress.data;
           //  this.global.hideLoader();
          
           this.getObsById();
            console.log(this.area);
           
          
            setTimeout(() => { 
              console.log(this.param.IDklasifikasi);        
              this.listSubArea(this.param.IDklasifikasi,1);
            },1800);
                     

        }
      );
      

  }


  listSubArea(idk,reload=0): void { 
    console.log(idk);
    var ID = idk;

    this.subarea = [];
    this.param.AreaID = '';

    this.global.showLoader();
    this.ObservasiProvider.listSubArea({KlarifikasiID:ID}).subscribe( ress =>
        {
            this.subarea = ress.data;
            this.global.hideLoader();
            console.log(this.subarea);


            this.getObsById();
        

            

        }
      );
  }


  listUnsafe(): void { 
   // this.global.showLoader();

    this.ObservasiProvider.listUnsafe().subscribe( ress =>
        {
            this.unsafe = ress.data;
            
           //  this.global.hideLoader();
            console.log(this.unsafe);
            console.log(this.unsafeDetailC);

            setTimeout(() => { 
              this.listUnsafeDetail(this.param.UnsafeID);
              },1500);

        }
      );
  }



  listUnsafeDetail(idk): void { 

    console.log(idk);
    var ID = idk;
 
    this.unsafedetail = [];
    this.param.unsafeDetailId = '';

    //this.global.showLoader();
    this.ObservasiProvider.listUnsDetail({UnsafeID:ID}).subscribe( ress =>
        {
            this.unsafedetail = ress.data;
            // this.global.hideLoader();
           // console.log(Object.entries(ress.data));
            console.log(this.unsafedetail);

      
    for (let [key, value] of Object.entries(this.unsafedetail)) {
       
     
      for (let [key2, value2] of Object.entries(value)) {
        var getVal: any = value2;
        
       
        for (let [key3, value3] of Object.entries(this.param.unsafedetail)) { 

          if(value3.Subksid == getVal.UnsDetail ) {
            this.unsafeDetailC[getVal.UnsDetail] = true;
            console.log(this.unsafeDetailC[getVal.UnsDetail]);
          }

        }
      
      }


      
    }
            

        }
      );
  }

  listPostCenter(): void { 
    // this.global.showLoader();
     this.ObservasiProvider.listPostCenter().subscribe( ress =>
         {
             this.CostCenter = ress.data;
            //  this.global.hideLoader();
             console.log(this.CostCenter);
         }
       );
   }


   simpan(proses): void{

    this.param.processApl = proses;

    if(this.param.processApl == 400){
      if(this.param.RejectReason == null) {
        this.error = 1;
        this.msg = 'Komentar Approval Tidak Boleh Kosong; \n';
       }
    }

    if(this.param.processApl == 900){
      if(this.param.RejectReason == null) {
        this.error = 1;
        this.msg = 'Reject Reason Tidak Boleh Kosong; \n';
       }
    }



   
   


    if(this.error == 1) {
      this.global.presentAlert('Error','Gagal Proses',this.msg);
      console.log(this.param.Pengelolahinfor);
      console.log(this.param.RejectReason);
    }
    else { 
  
      this.param.IDobservasion = this.IDobservasion;

    
  
   console.log(this.param);
    this.global.showLoader();
    
    try {
      this.ObservasiProvider.update(this.param).subscribe( ress =>
        {
          this.global.hideLoader();
          this.router.navigate(['/home']);
          console.log(ress);
        }
      );
  } catch (error) {
      this.global.presentAlert('Error','Review Data Gagal',' — Error is handled gracefully: '+ error.name);
    }
    
  
    }

     

    }


    cancel(): void{
      this.router.navigateByUrl('/home');
    }

}
