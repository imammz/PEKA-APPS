import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-redirect',
  templateUrl: './redirect.page.html',
  styleUrls: ['./redirect.page.scss'],
})
export class RedirectPage implements OnInit {

  login: any = '';
  userData: any = '';

  constructor(private router: Router,
    public storage: Storage,
    public global: GlobalService) { }

  ngOnInit() {
   
    this.storage.get('login').then((val) => {
      this.login = val;
      this.router.navigate(['/home']);
    });
    this.storage.get('userData').then((val) => {
      this.userData = val;
    });


    


  }

}
