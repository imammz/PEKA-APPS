import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ObservasiService } from './../services/observasi.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/Camera/ngx';
import { ActionSheetController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { HttpClient } from '@angular/common/http';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Storage } from '@ionic/storage';
import { FilePath } from '@ionic-native/file-path/ngx';
 
const today = new Date();
const STORAGE_KEY = 'edit_obs_'+today.getFullYear()+''+(today.getMonth()+1)+''+today.getDate()+today.getHours() + '' + today.getMinutes() + '' + today.getSeconds();

@Component({
  selector: 'app-edit-obs',
  templateUrl: './edit-obs.page.html',
  styleUrls: ['./edit-obs.page.scss'],
})
export class EditObsPage implements OnInit {

 
  loader = null;
  KlarifikasiID = '';
  UnsafeID= '';
  userData: any = [];
  error = 0;
  msg = '';
  IDobservasion = '';
  opsi = "0";

  area = [];
  subarea = [];
  unsafe=[];
  unsafedetail=[];
  pegawai=[];
  CostCenter = [];
  jenisPekerja = 'pekerja';

  unsafedetailVal = [];
  processApl = 0;
  indexunsafe = 0;

  unsafeDetailC = [];

  arrayImg = [];
  arrayImgName = [];

  slideimg = [];
  updateFoto = false;

  param = {
    IDklasifikasi:'',
    spesifikasi:'',
    DateObs:'',
    Pengamatan:'',
    Lanjutan:'',
    Klasifikasi:'',
    FilePhoto:'',
    IDNIK:'',
    NamaEmploye:'',
    FungsiName:'',
    Email:'',
    NoTlp:'',
    CreateID:'',
    AreaID:'',
    jabatan:'',
    processApl:0,
    langsung:'',
    unsafeDetailId:'',
    CostCenter:'',
    UnsafeID:'',
    lokasi_tempat:'',
    processApl_desc: '',
    IDobservasion: '',
    IsActive: null,
    CreateDate: '',
    PICNIK: null,
    PICSign: null,
    PISignDate: '',
    PICEmail: null,
    PICInformasi: null,
    RiskA: null,
    RiskB: null,
    RejectReason: null,
    Pengelolahinfor: null,
    UserBypass: null,
    BypassDate: '',
    Aksi: null,
    AksiDate: '',
    AksiComment: null,
    unsafedetail: []
  };



  constructor(
    private routeAct: ActivatedRoute,
    private ObservasiProvider: ObservasiService,
    public loadingController: LoadingController,
    private router: Router,
    public global: GlobalService,
    private storage: Storage,
    private camera: Camera, private file: File, private http: HttpClient, private webview: WebView,
    private actionSheetController: ActionSheetController, private toastController: ToastController,
     private platform: Platform, 
    private ref: ChangeDetectorRef, private filePath: FilePath    
    ) { }


    images = [];

  
  ngOnInit() {

    this.IDobservasion = this.routeAct.snapshot.paramMap.get("id");
    this.opsi = this.routeAct.snapshot.paramMap.get("opsi");
    
    console.log(this.IDobservasion);

    this.listArea();
    this.listUnsafe();
    this.listEmploye();
    this.listPostCenter();

    this.storage.get('userData').then((val) => {
      this.userData = val;
      this.param.CreateID    = this.userData.UserName;
      this.param.Email    = this.userData.Email;
    });
 
  }

  croppedImagepath = "";
  isLoading = false;

  imagePickerOptions = {
    maximumImagesCount: 1,
    quality: 50
  };

  
  loadStoredImages() {
    this.storage.get(STORAGE_KEY).then(images => {
      if (images) {
        let arr = JSON.parse(images);
        this.images = [];
        for (let img of arr) {
          let filePath = this.file.dataDirectory + img;
          let resPath = this.pathForImage(filePath);
          this.images.push({ name: img, path: resPath, filePath: filePath });
        }
      }
    });
  }
 
  pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      let converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }
 
  async presentToast(text) {
    const toast = await this.toastController.create({
        message: text,
        position: 'bottom',
        duration: 3000
    });
    toast.present();
  }

  pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then(imagePath => {
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
          this.filePath.resolveNativePath(imagePath)
              .then(filePath => {
                  let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                  let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                  this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
              });
      } else {
          var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
          var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
          this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
  });
  }

  async selectImage() {

    if(this.images.length > 2) {
      alert('Maksimum Gambar 3');
    }
    else { 
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();


    setTimeout(() => { 
      this.upload();
    },1500);


    }
  }


  takePicture(sourceType: PictureSourceType) {
    var options: CameraOptions = {
        quality: 100,
        sourceType: sourceType,
        saveToPhotoAlbum: false,
        correctOrientation: true
    };
 
    this.camera.getPicture(options).then(imagePath => {
        if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
            this.filePath.resolveNativePath(imagePath)
                .then(filePath => {
                    let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
                });
        } else {
            var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        }
    });
 
}
  createFileName() {
    var d = new Date(),
        n = d.getTime(),
        newFileName = n + ".png";
    return newFileName;
}
 
copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
        this.updateStoredImages(newFileName);
    }, error => {
        this.presentToast('Error while storing file.');
    });
}
 
updateStoredImages(name) {
    this.storage.get(STORAGE_KEY).then(images => {
        let arr = JSON.parse(images);
        if (!arr) {
            let newImages = [name];
            this.storage.set(STORAGE_KEY, JSON.stringify(newImages));
        } else {
            arr.push(name);
            this.storage.set(STORAGE_KEY, JSON.stringify(arr));
        }
 
        let filePath = this.file.dataDirectory + name;
        let resPath = this.pathForImage(filePath);
 
        let newEntry = {
            name: name,
            path: resPath,
            filePath: filePath
        };
 
        this.images = [newEntry, ...this.images];
        this.ref.detectChanges(); // trigger change detection cycle
    });
}


deleteImage(imgEntry, position) {
    this.images.splice(position, 1);
 
    this.storage.get(STORAGE_KEY).then(images => {
        let arr = JSON.parse(images);
        let filtered = arr.filter(name => name != imgEntry.name);
        this.storage.set(STORAGE_KEY, JSON.stringify(filtered));
 
        var correctPath = imgEntry.filePath.substr(0, imgEntry.filePath.lastIndexOf('/') + 1);
 
        this.file.removeFile(correctPath, imgEntry.name).then(res => {
            this.presentToast('File removed.');
        });
    });
}

 
startUpload(imgEntry) {
  this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
      .then(entry =>  {
          ( < FileEntry > entry).file(file => this.readFile(file))
      })
      .catch(err => {
          this.presentToast('Error while reading file.');
      });
}

readFile(file: any) {
  const reader = new FileReader();
  reader.onload = () => {
      const formData = new FormData();
      const imgBlob = new Blob([reader.result], {
          type: file.type
      });

   



    
  };
  reader.readAsArrayBuffer(file);
}


  getObsById(): void { 
    this.global.showLoader();
    var data = {IDobservasion: this.IDobservasion};
    
    this.ObservasiProvider.get(data).subscribe( ress =>
        {
            this.param = ress.data[0];
            this.global.hideLoader();
            console.log(this.param); 

            this.param.UnsafeID = this.param.Klasifikasi;

            this.slideimg = this.param.FilePhoto.split(',');          


        }
      );
  }


  listArea(): void { 
   // this.global.showLoader();


      this.ObservasiProvider.listArea().subscribe( ress =>
        {
            this.area = ress.data;
           //  this.global.hideLoader();
          
           this.getObsById();
            console.log(this.area);
           
          
            setTimeout(() => { 
              console.log(this.param.IDklasifikasi);        
              this.listSubArea(this.param.IDklasifikasi,1);
            },2000);
                     

        }
      );
      

  }


  listSubArea(idk,reload=0): void { 
    console.log(idk);
    var ID = idk;

    this.subarea = [];
    this.param.AreaID = '';

    this.global.showLoader();
    this.ObservasiProvider.listSubArea({KlarifikasiID:ID}).subscribe( ress =>
        {
            this.subarea = ress.data;
            this.global.hideLoader();
            console.log(this.subarea);

            if(reload != 0) {
              this.getObsById();
            }
           
        

            

        }
      );
  }


  listUnsafe(): void { 
   // this.global.showLoader();

    this.ObservasiProvider.listUnsafe().subscribe( ress =>
        {
            this.unsafe = ress.data;
            
           //  this.global.hideLoader();
            console.log(this.unsafe);
            console.log(this.unsafeDetailC);


          //  this.getObsById();
          
          
            setTimeout(() => { 
            this.listUnsafeDetail(this.param.UnsafeID);
            },1500);
        }
      );
  }


  listUnsafeDetail(idk): void { 

    console.log(idk);
    var ID = idk;
 
    this.unsafedetail = [];
    this.param.unsafeDetailId = '';

    //this.global.showLoader();
    this.ObservasiProvider.listUnsDetail({UnsafeID:ID}).subscribe( ress =>
        {
            this.unsafedetail = ress.data;
            // this.global.hideLoader();
           // console.log(Object.entries(ress.data));
            console.log(this.unsafedetail);

      
    for (let [key, value] of Object.entries(this.unsafedetail)) {
       
     
      for (let [key2, value2] of Object.entries(value)) {
        var getVal: any = value2;
        
       
        for (let [key3, value3] of Object.entries(this.param.unsafedetail)) { 

          if(value3.Subksid == getVal.UnsDetail ) {
            this.unsafeDetailC[getVal.UnsDetail] = true;

          }

        }
      
      }


      
    }
            

        }
      );
  }

  listEmploye(): void { 
   // this.global.showLoader();
    this.ObservasiProvider.listEmployee().subscribe( ress =>
        {
            this.pegawai = ress.data;
           //  this.global.hideLoader();
            console.log(this.pegawai);

            this.param.NamaEmploye = this.userData.Nama;

        }
      );
  }


  listPostCenter(): void { 
    // this.global.showLoader();
     this.ObservasiProvider.listPostCenter().subscribe( ress =>
         {
             this.CostCenter = ress.data;
            //  this.global.hideLoader();
             console.log(this.CostCenter);
         }
       );
   }

   upload(): void{
    
    for (let img of this.images) { 
      this.startUpload(img);
   }

   this.param.FilePhoto = this.arrayImgName.join(',');

   //alert(this.param.FilePhoto);

   } 



   simpan(proses): void{

    if(this.param.IDklasifikasi == '') {
        this.error = 1;
        this.msg += 'Area Tidak Boleh Kosong; \n';
    }
    if(this.param.spesifikasi == '') {
        this.error = 1;
        this.msg += 'Lokasi Tidak Boleh Kosong; \n';
    }
    if(this.param.NamaEmploye == '') {
        this.error = 1;
        this.msg += 'Nama Tidak Boleh Kosong; \n';
    }


    if(this.error == 1) {
      this.global.presentAlert('Error','Gagal Edit',this.msg);
    }
    else {
        
    var tempUnsafedetail = [];
    this.param.processApl = proses;

    var i= 0;
    for (let [key, value] of Object.entries(this.unsafeDetailC)) {
       tempUnsafedetail[i] = key;
       i++;
    }

    this.param.unsafeDetailId = tempUnsafedetail.toString();
    this.param.Klasifikasi = this.param.UnsafeID;

    this.param.IDobservasion = this.IDobservasion;

    

   const formData = new FormData();


   Object.entries(this.images).
   forEach(([key, value]) => { 
     
     console.log('File : '+JSON.stringify(key, null, 4)); 
     console.log(JSON.stringify(value, null, 4)); 
    
     if(key == '0') {

       this.file.resolveLocalFilesystemUrl(value.filePath)
       .then(entry =>  {
           ( < FileEntry > entry).file(file => {
             const reader = new FileReader();
             reader.onload = () => {
                
                 const imgBlob = new Blob([reader.result], {
                     type: file.type
                 });
              formData.append('attach_file_1', imgBlob, file.name);
              console.log('Key '+key+' - '+formData); 
              console.log(JSON.stringify(imgBlob, null, 4)); 
             };
             reader.readAsArrayBuffer(file);
           })
       })
       .catch(err => {
           this.presentToast('Error while reading file.');
       });

     } 
     if(key == '1') {
       this.file.resolveLocalFilesystemUrl(value.filePath)
       .then(entry =>  {
           ( < FileEntry > entry).file(file => {
             const reader = new FileReader();
             reader.onload = () => {
              
                 const imgBlob = new Blob([reader.result], {
                     type: file.type
                 });
              formData.append('attach_file_2', imgBlob, file.name);
              console.log('Key '+key+' - '+formData);
              console.log(JSON.stringify(imgBlob, null, 4)); 
             };
             reader.readAsArrayBuffer(file);
           })
       })
       .catch(err => {
           this.presentToast('Error while reading file.');
       });
     }
     if(key == '2') {
       this.file.resolveLocalFilesystemUrl(value.filePath)
       .then(entry =>  {
           ( < FileEntry > entry).file(file => {
             const reader = new FileReader();
             reader.onload = () => {

                 const imgBlob = new Blob([reader.result], {
                     type: file.type
                 });
              formData.append('attach_file_3', imgBlob, file.name);
              console.log('Key '+key+' - '+formData);
              console.log(JSON.stringify(imgBlob, null, 4)); 
             };
             reader.readAsArrayBuffer(file);
           })
       })
       .catch(err => {
           this.presentToast('Error while reading file.');
       });
     }    
   });

     setTimeout(() => { 
    this.global.showLoader();
    try {

      formData.append('IDobservasion', this.param.IDobservasion);
      formData.append('IDklasifikasi', this.param.IDklasifikasi);
      formData.append('Klasifikasi', this.param.Klasifikasi);
      formData.append('unsafeDetailId', this.param.unsafeDetailId);
      /*
      
     
      formData.append('spesifikasi', this.param.spesifikasi);
      formData.append('DateObs', this.param.DateObs);
      formData.append('Pengamatan', this.param.Pengamatan);
      formData.append('Lanjutan', this.param.Lanjutan);
      
      formData.append('FilePhoto', this.param.FilePhoto);
      formData.append('lokasi_tempat', this.param.lokasi_tempat);
      formData.append('processApl_desc', this.param.processApl_desc);
     
      formData.append('IDNIK', this.param.IDNIK);
      formData.append('NamaEmploye', this.param.NamaEmploye);
      formData.append('FungsiName', this.param.FungsiName);
      formData.append('Email', this.param.Email);
      formData.append('NoTlp', this.param.NoTlp);
      formData.append('CreateID', this.param.CreateID);
      formData.append('IsActive', this.param.IsActive);
      formData.append('CreateDate', this.param.CreateDate);
      formData.append('CreateID', this.param.CreateID);
      formData.append('AreaID', this.param.AreaID);
     
      formData.append('jabatan', this.param.jabatan);
      formData.append('processApl', this.param.processApl);

      formData.append('langsung', this.param.langsung);
      formData.append('PICNIK', this.param.PICNIK);
      formData.append('PICSign', this.param.PICSign);
      formData.append('PISignDate', this.param.PISignDate);
      formData.append('PICEmail', this.param.PICEmail);
      formData.append('PICInformasi', this.param.PICInformasi);
      formData.append('RiskA', this.param.RiskA);
      formData.append('RiskB', this.param.RiskB);
      formData.append('RejectReason', this.param.RejectReason);
      formData.append('Pengelolahinfor', this.param.Pengelolahinfor);
      formData.append('UserBypass', this.param.UserBypass);
      formData.append('BypassDate', this.param.BypassDate);
      formData.append('Aksi', this.param.Aksi);
      formData.append('AksiDate', this.param.AksiDate);
      formData.append('AksiComment', this.param.AksiComment);
      
      formData.append('CostCenter', this.param.CostCenter);
      formData.append('UnsafeID', this.param.UnsafeID);
      
     
      */
      console.log('Kirim : '+formData);

      this.ObservasiProvider.update(formData).subscribe( ress =>
        {
          console.log(ress);

          this.ObservasiProvider.update(this.param).subscribe( ress =>
            {
              this.global.hideLoader();
              //this.router.navigate(['/home']);
              this.router.navigateByUrl('/home', { replaceUrl: true });
              
              console.log(ress);
            }
          );
        }
      );

    



  } catch (error) {
      this.global.presentAlert('Error','Edit Data Gagal',' — Error is handled gracefully: '+ error.name);
    }

  }, 3000);


  
    }

     

    }



    comboEmploye(id): void { 
      this.pegawai.forEach(el => {
         if(el.Nama==id) {
           this.param.Email = el.Email;
         }
      });
   }

  cancel(): void{
    this.router.navigateByUrl('/home');
  }
  

}
