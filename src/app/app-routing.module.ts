import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'config',
    loadChildren: () => import('./config/config.module').then( m => m.ConfigPageModule)
  },
  {
    path: 'tambah-obs',
    loadChildren: () => import('./tambah-obs/tambah-obs.module').then( m => m.TambahObsPageModule)
  },
  {
    path: 'detail-obs/:id',
    loadChildren: () => import('./detail-obs/detail-obs.module').then( m => m.DetailObsPageModule)
  },
  {
    path: 'hapus-obs/:id',
    loadChildren: () => import('./hapus-obs/hapus-obs.module').then( m => m.HapusObsPageModule)
  },
  {
    path: 'edit-obs/:id',
    loadChildren: () => import('./edit-obs/edit-obs.module').then( m => m.EditObsPageModule)
  },
  {
    path: 'edit-obs/:id/:opsi',
    loadChildren: () => import('./edit-obs/edit-obs.module').then( m => m.EditObsPageModule)
  },
  {
    path: 'set-pic-obs/:id',
    loadChildren: () => import('./set-pic-obs/set-pic-obs.module').then( m => m.SetPicObsPageModule)
  },
  {
    path: 'redirect',
    loadChildren: () => import('./redirect/redirect.module').then( m => m.RedirectPageModule)
  },
  {
    path: 'tindak-lanjut-pic/:id',
    loadChildren: () => import('./tindak-lanjut-pic/tindak-lanjut-pic.module').then( m => m.TindakLanjutPicPageModule)
  },
  {
    path: 'review-tl/:id',
    loadChildren: () => import('./review-tl/review-tl.module').then( m => m.ReviewTlPageModule)
  },
  {
    path: 'logout',
    loadChildren: () => import('./logout/logout.module').then( m => m.LogoutPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
