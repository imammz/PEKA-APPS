import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ObservasiService } from './../services/observasi.service';
import { GlobalService } from '../services/global.service';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/Camera/ngx';
import { ActionSheetController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { HttpClient } from '@angular/common/http';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Storage } from '@ionic/storage';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Router, ActivatedRoute } from '@angular/router';

const STORAGE_KEY = 'my_images';
@Component({
  selector: 'app-tindak-lanjut-pic',
  templateUrl: './tindak-lanjut-pic.page.html',
  styleUrls: ['./tindak-lanjut-pic.page.scss'],
})
export class TindakLanjutPicPage implements OnInit {


  loader = null;
  KlarifikasiID = '';
  UnsafeID= '';
  userData: any = [];
  error = 0;
  msg = '';
  IDobservasion = '';
  opsi = "0";

  area = [];
  subarea = [];
  unsafe=[];
  unsafedetail=[];
  pegawai=[];
  CostCenter = [];
  jenisPekerja = 'pekerja';

  unsafedetailVal = [];
  processApl = 0;
  indexunsafe = 0;

  unsafeDetailC = [];


  param = {
    IDklasifikasi:'',
    spesifikasi:'',
    DateObs:'',
    Pengamatan:'',
    Lanjutan:'',
    Klasifikasi:'',
    FilePhoto:'',
    IDNIK:'',
    NamaEmploye:'',
    FungsiName:'',
    Email:'',
    NoTlp:'',
    CreateID:'',
    AreaID:'',
    jabatan:'',
    processApl:0,
    langsung:'',
    unsafeDetailId:'',
    CostCenter:'',
    UnsafeID:'',
    lokasi_tempat:'',
    processApl_desc: '',
    IDobservasion: '',
    IsActive: null,
    CreateDate: '',
    PICNIK: null,
    PICSign: null,
    PISignDate: '',
    PICEmail: 'A',
    PICInformasi: null,
    RiskA: null,
    RiskB: null,
    RejectReason: null,
    Pengelolahinfor: null,
    UserBypass: null,
    BypassDate: '',
    Aksi: null,
    AksiDate: '',
    AksiComment: null,
    unsafedetail: []
  };

  constructor(
    private routeAct: ActivatedRoute,
    private ObservasiProvider: ObservasiService,
    public loadingController: LoadingController,
    private router: Router,
    private global: GlobalService,
    private storage: Storage,
    private camera: Camera, private file: File, private http: HttpClient, private webview: WebView,
    private actionSheetController: ActionSheetController, private toastController: ToastController,
     private platform: Platform, 
    private ref: ChangeDetectorRef, private filePath: FilePath
    ) { }

    images = [];

  ngOnInit() {

    this.IDobservasion = this.routeAct.snapshot.paramMap.get("id");
    this.opsi = '1';

    
    console.log(this.IDobservasion);

    this.listArea();
    this.listUnsafe();
    this.listPostCenter();

    this.storage.get('userData').then((val) => {
      this.userData = val;
      console.log(this.userData.Email);
      this.param.CreateID    = this.userData.UserName;
      this.param.PICEmail    = this.userData.Email;
    });




    this.platform.ready().then(() => {
      this.loadStoredImages();
    });
    

  }



  
  croppedImagepath = "";
  isLoading = false;

  imagePickerOptions = {
    maximumImagesCount: 1,
    quality: 50
  };


  loadStoredImages() {
    this.storage.get(STORAGE_KEY).then(images => {
      if (images) {
        let arr = JSON.parse(images);
        this.images = [];
        for (let img of arr) {
          let filePath = this.file.dataDirectory + img;
          let resPath = this.pathForImage(filePath);
          this.images.push({ name: img, path: resPath, filePath: filePath });
        }
      }
    });
  }
 
  pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      let converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }
 
  async presentToast(text) {
    const toast = await this.toastController.create({
        message: text,
        position: 'bottom',
        duration: 3000
    });
    toast.present();
  }

  pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then(imagePath => {
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
          this.filePath.resolveNativePath(imagePath)
              .then(filePath => {
                  let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                  let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                  this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
              });
      } else {
          var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
          var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
          this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
  });
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }


  createFileName() {
    var d = new Date(),
        n = d.getTime(),
        newFileName = n + ".jpg";
    return newFileName;
}
 
copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
        this.updateStoredImages(newFileName);
    }, error => {
        this.presentToast('Error while storing file.');
    });
}
 
updateStoredImages(name) {
    this.storage.get(STORAGE_KEY).then(images => {
        let arr = JSON.parse(images);
        if (!arr) {
            let newImages = [name];
            this.storage.set(STORAGE_KEY, JSON.stringify(newImages));
        } else {
            arr.push(name);
            this.storage.set(STORAGE_KEY, JSON.stringify(arr));
        }
 
        let filePath = this.file.dataDirectory + name;
        let resPath = this.pathForImage(filePath);
 
        let newEntry = {
            name: name,
            path: resPath,
            filePath: filePath
        };
 
        this.images = [newEntry, this.images];
        this.ref.detectChanges(); // trigger change detection cycle
    });
}


deleteImage(imgEntry, position) {
    this.images.splice(position, 1);
 
    this.storage.get(STORAGE_KEY).then(images => {
        let arr = JSON.parse(images);
        let filtered = arr.filter(name => name != imgEntry.name);
        this.storage.set(STORAGE_KEY, JSON.stringify(filtered));
 
        var correctPath = imgEntry.filePath.substr(0, imgEntry.filePath.lastIndexOf('/') + 1);
 
        this.file.removeFile(correctPath, imgEntry.name).then(res => {
            this.presentToast('File removed.');
        });
    });
}

 
startUpload(imgEntry) {
  this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
      .then(entry => {
          ( < FileEntry > entry).file(file => this.readFile(file))
      })
      .catch(err => {
          this.presentToast('Error while reading file.');
      });
}

readFile(file: any) {
  const reader = new FileReader();
  reader.onload = () => {
      const formData = new FormData();
      const imgBlob = new Blob([reader.result], {
          type: file.type
      });
      formData.append('file', imgBlob, file.name);
      this.uploadImageData(formData);
  };
  reader.readAsArrayBuffer(file);
}

async uploadImageData(formData: FormData) {
 
}

  
  
  getObsById(): void { 
    this.global.showLoader();
    var data = {IDobservasion: this.IDobservasion};
    
    this.ObservasiProvider.get(data).subscribe( ress =>
        {
            this.param = ress.data[0];
            this.global.hideLoader();
            console.log(this.param); 

            this.param.UnsafeID = this.param.Klasifikasi;
            this.param.PICEmail = this.userData.Email;
          


        }
      );
  }


  listArea(): void { 
   // this.global.showLoader();


      this.ObservasiProvider.listArea().subscribe( ress =>
        {
            this.area = ress.data;
           //  this.global.hideLoader();
          
           this.getObsById();
            console.log(this.area);
           
          
            setTimeout(() => { 
              console.log(this.param.IDklasifikasi);        
              this.listSubArea(this.param.IDklasifikasi,1);
            },1800);
                     

        }
      );
      

  }


  listSubArea(idk,reload=0): void { 
    console.log(idk);
    var ID = idk;

    this.subarea = [];
    this.param.AreaID = '';

    this.global.showLoader();
    this.ObservasiProvider.listSubArea({KlarifikasiID:ID}).subscribe( ress =>
        {
            this.subarea = ress.data;
            this.global.hideLoader();
            console.log(this.subarea);


            this.getObsById();
      
            

        }
      );
  }


  listUnsafe(): void { 
   // this.global.showLoader();

    this.ObservasiProvider.listUnsafe().subscribe( ress =>
        {
            this.unsafe = ress.data;
            
           //  this.global.hideLoader();
            console.log(this.unsafe);
            console.log(this.unsafeDetailC);

            setTimeout(() => { 
              this.listUnsafeDetail(this.param.UnsafeID);
              },1500);

        }
      );
  }


  listUnsafeDetail(idk): void { 

    console.log(idk);
    var ID = idk;
 
    this.unsafedetail = [];
    this.param.unsafeDetailId = '';

    //this.global.showLoader();
    this.ObservasiProvider.listUnsDetail({UnsafeID:ID}).subscribe( ress =>
        {
            this.unsafedetail = ress.data;
            // this.global.hideLoader();
           // console.log(Object.entries(ress.data));
            console.log(this.unsafedetail);

      
    for (let [key, value] of Object.entries(this.unsafedetail)) {
       
     
      for (let [key2, value2] of Object.entries(value)) {
        var getVal: any = value2;
        
       
        for (let [key3, value3] of Object.entries(this.param.unsafedetail)) { 

          if(value3.Subksid == getVal.UnsDetail ) {
            this.unsafeDetailC[getVal.UnsDetail] = true;

          }

        }
      
      }


      
    }
            

        }
      );
  }



  listPostCenter(): void { 
    // this.global.showLoader();
     this.ObservasiProvider.listPostCenter().subscribe( ress =>
         {
             this.CostCenter = ress.data;
            //  this.global.hideLoader();
             console.log(this.CostCenter);
         }
       );
   }


   simpan(proses): void{

    this.param.processApl = proses;

    this.param.IDobservasion = this.IDobservasion;

   console.log(this.param);
    this.global.showLoader();
    
    try {
      this.ObservasiProvider.update(this.param).subscribe( ress =>
        {
          this.global.hideLoader();
          this.router.navigate(['/home']);
          console.log(ress);
        }
      );
  } catch (error) {
      this.global.presentAlert('Error','Tindak Lanjut Data Gagal',' — Error is handled gracefully: '+ error.name);
    }
    
  
    

     

    }


    cancel(): void{
      this.router.navigateByUrl('/home');
    }


}
